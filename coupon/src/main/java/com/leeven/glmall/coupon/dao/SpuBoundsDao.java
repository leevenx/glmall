package com.leeven.glmall.coupon.dao;

import com.leeven.glmall.coupon.entity.SpuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品spu积分设置
 * 
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 20:05:40
 */
@Mapper
public interface SpuBoundsDao extends BaseMapper<SpuBoundsEntity> {
	
}
