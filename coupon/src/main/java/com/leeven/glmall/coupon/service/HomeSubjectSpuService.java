package com.leeven.glmall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leeven.common.utils.PageUtils;
import com.leeven.glmall.coupon.entity.HomeSubjectSpuEntity;

import java.util.Map;

/**
 * 专题商品
 *
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 20:05:40
 */
public interface HomeSubjectSpuService extends IService<HomeSubjectSpuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

