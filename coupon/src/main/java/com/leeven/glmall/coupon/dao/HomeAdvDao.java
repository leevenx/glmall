package com.leeven.glmall.coupon.dao;

import com.leeven.glmall.coupon.entity.HomeAdvEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 首页轮播广告
 * 
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 20:05:40
 */
@Mapper
public interface HomeAdvDao extends BaseMapper<HomeAdvEntity> {
	
}
