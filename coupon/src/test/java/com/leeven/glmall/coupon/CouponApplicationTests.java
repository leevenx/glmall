package com.leeven.glmall.coupon;

import com.leeven.glmall.coupon.entity.CouponEntity;
import com.leeven.glmall.coupon.service.CouponService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class CouponApplicationTests {

    @Autowired
    CouponService couponService;

    @Test
    void contextLoads() {
        List<CouponEntity> list = couponService.list();
        list.forEach(System.out::println);
    }

}
