package com.leeven.glmall.thirdparty;

import com.aliyun.oss.OSS;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@SpringBootTest
class ThirdPartyApplicationTests {

    @Autowired
    OSS ossClient;

    /**
     * 1.添加依赖：
     * <dependency>
     * <groupId>com.alibaba.cloud</groupId>
     * <artifactId>spring-cloud-starter-alicloud-oss</artifactId>
     * <version>2.2.0.RELEASE</version>
     * </dependency>
     * <p>
     * 2.配置文件：
     * 3.注入 @Autowired
     * OSS ossClient;
     * 4.调用API
     */
    @Test
    void ossUploadTest() throws FileNotFoundException {
        // 上传文件流。
        InputStream inputStream = new FileInputStream("C:\\Users\\Administrator\\Pictures\\GC算法.png");
        ossClient.putObject("leeven-glmall", "GC算法2.png", inputStream);
        // 关闭OSSClient。
        ossClient.shutdown();

        System.out.println("上传成功");
    }

    @Value("${server.port}")
    private String port;

    @Test
    void test() {
        System.out.println("port:" + port);
    }

}
