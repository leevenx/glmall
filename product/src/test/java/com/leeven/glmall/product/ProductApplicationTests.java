package com.leeven.glmall.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.leeven.glmall.product.entity.BrandEntity;
import com.leeven.glmall.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Test
    void contextLoads() {
        BrandEntity brand = new BrandEntity();
        brand.setName("华为");
        brandService.save(brand);
    }

    @Test
    void get() {
        List<BrandEntity> brands = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));
        brands.forEach((b) -> System.out.println(b));
    }

}
