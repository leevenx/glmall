package com.leeven.glmall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leeven.common.utils.PageUtils;
import com.leeven.glmall.product.entity.BrandEntity;

import java.util.Map;

/**
 * 品牌
 *
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 19:02:35
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

