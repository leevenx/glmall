package com.leeven.glmall.product.dao;

import com.leeven.glmall.product.entity.ProductAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu属性值
 * 
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 19:02:34
 */
@Mapper
public interface ProductAttrValueDao extends BaseMapper<ProductAttrValueEntity> {
	
}
