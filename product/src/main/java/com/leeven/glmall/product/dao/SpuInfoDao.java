package com.leeven.glmall.product.dao;

import com.leeven.glmall.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息
 * 
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 19:02:35
 */
@Mapper
public interface SpuInfoDao extends BaseMapper<SpuInfoEntity> {
	
}
