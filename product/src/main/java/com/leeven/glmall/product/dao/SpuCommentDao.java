package com.leeven.glmall.product.dao;

import com.leeven.glmall.product.entity.SpuCommentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品评价
 * 
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 19:02:34
 */
@Mapper
public interface SpuCommentDao extends BaseMapper<SpuCommentEntity> {
	
}
