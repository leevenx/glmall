package com.leeven.glmall.product.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.leeven.common.utils.PageUtils;
import com.leeven.common.utils.Query;

import com.leeven.glmall.product.dao.CategoryDao;
import com.leeven.glmall.product.entity.CategoryEntity;
import com.leeven.glmall.product.service.CategoryService;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listTree() {
        List<CategoryEntity> categories = baseMapper.selectList(null);

        List<CategoryEntity> levelOne = categories.stream().filter(c -> {
            // 先查找一级菜单
            return c.getParentCid() == 0;
        }).map(c -> {
            c.setChildren(getChildrens(c, categories));
            return c;
        }).sorted((c1, c2) -> {
            return (c1.getSort() == null ? 0 : c1.getSort()) - (c2.getSort() == null ? 0 : c2.getSort());
        }).collect(Collectors.toList());
        return levelOne;
    }

    @Override
    public void deleteCategoryByIds(List<Long> ids) {
        // TODO 检查当前删除的分类在别的地方是否用到，并且没有下级分类
        baseMapper.deleteBatchIds(ids);
    }

    private List<CategoryEntity> getChildrens(CategoryEntity parent, List<CategoryEntity> categorys) {
        return categorys.stream().filter(c -> {
            // 查找当前菜单的父id和上级的id一致的菜单，过滤掉不等的
            return c.getParentCid() == parent.getCatId();
        }).map(c -> {
            // 再为当前的子菜单设置子菜单。
            c.setChildren(getChildrens(c, categorys));
            return c;
        }).sorted((c1, c2) -> {
            return (c1.getSort() == null ? 0 : c1.getSort()) - (c2.getSort() == null ? 0 : c2.getSort());
        }).collect(Collectors.toList());
    }

}