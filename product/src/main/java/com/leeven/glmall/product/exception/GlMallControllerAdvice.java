package com.leeven.glmall.product.exception;

import com.leeven.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@ResponseBody
@ControllerAdvice(basePackages = "com.leeven.glmall.product.controller")
public class GlMallControllerAdvice {

    /**
     * 统一处理校验异常
     * @param e controller抛出 MethodArgumentNotValidException 异常后，会执行这个方法。
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public R handleValidateException(MethodArgumentNotValidException e) {
        HashMap<String, String> map = new HashMap<>();
        BindingResult br = e.getBindingResult();
        List<FieldError> fieldErrors = br.getFieldErrors();
        for (FieldError fieldError : fieldErrors) {
            String msg = fieldError.getDefaultMessage();
            String field = fieldError.getField();
            map.put(field, msg);
        }
        return R.error(400, "参数不合法").put("data", map);
    }

    /**
     * 上面精确匹配异常处理不了的异常都会在这里处理。
     */
    @ExceptionHandler(Throwable.class)
    public R handleException(Throwable e) {
        log.error("服务异常", e);
        return R.error();
    }
}
