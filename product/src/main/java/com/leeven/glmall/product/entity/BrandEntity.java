package com.leeven.glmall.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.leeven.common.validate.AddGroup;
import com.leeven.common.validate.UpdateGroup;
import com.leeven.common.validate.UpdateStatusGroup;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * 品牌
 *
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 19:02:35
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 品牌id
     */
    @TableId
    @Null(groups = AddGroup.class, message = "新增不能指定id")
    @NotBlank(groups = UpdateGroup.class, message = "修改时，id不能为空")
    private Long brandId;
    /**
     * 品牌名
     */
    @NotBlank(message = "品牌名不能为空", groups = {AddGroup.class, UpdateGroup.class})
    private String name;
    /**
     * 品牌logo地址
     */
    @URL(message = "logo地址不合法", groups = {AddGroup.class, UpdateGroup.class})
    private String logo;
    /**
     * 介绍
     */
    private String descript;
    /**
     * 显示状态[0-不显示；1-显示]
     *
     * @Pattern 不能用在 integer 类型上
     */
    @Min(value = 0, groups = {UpdateStatusGroup.class, AddGroup.class, UpdateGroup.class})
    @Max(value = 1, groups = {UpdateStatusGroup.class, AddGroup.class, UpdateGroup.class})
    @NotNull(groups = {UpdateStatusGroup.class, AddGroup.class, UpdateGroup.class})
    private Integer showStatus;
    /**
     * 检索首字母
     */
    @Pattern(regexp = "^[a-zA-Z]", groups = {AddGroup.class, UpdateGroup.class})
    private String firstLetter;
    /**
     * 排序
     */
    @Min(value = 0, groups = {AddGroup.class})
    @NotNull
    private Integer sort;

}
