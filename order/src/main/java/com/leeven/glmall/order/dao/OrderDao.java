package com.leeven.glmall.order.dao;

import com.leeven.glmall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 20:10:27
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
