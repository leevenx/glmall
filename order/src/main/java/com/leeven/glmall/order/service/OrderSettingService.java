package com.leeven.glmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leeven.common.utils.PageUtils;
import com.leeven.glmall.order.entity.OrderSettingEntity;

import java.util.Map;

/**
 * 订单配置信息
 *
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 20:10:27
 */
public interface OrderSettingService extends IService<OrderSettingEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

