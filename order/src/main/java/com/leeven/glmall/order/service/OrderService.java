package com.leeven.glmall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leeven.common.utils.PageUtils;
import com.leeven.glmall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 20:10:27
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

