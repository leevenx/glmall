package com.leeven.glmall.order;

import com.leeven.glmall.order.entity.OrderEntity;
import com.leeven.glmall.order.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class OrderApplicationTests {

    @Autowired
    OrderService orderService;

    @Test
    void contextLoads() {
        List<OrderEntity> list = orderService.list();
        list.forEach(System.out::println);
    }

}
