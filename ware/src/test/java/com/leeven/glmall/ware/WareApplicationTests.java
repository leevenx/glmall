package com.leeven.glmall.ware;

import com.leeven.glmall.ware.entity.WareInfoEntity;
import com.leeven.glmall.ware.service.WareInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class WareApplicationTests {

    @Autowired
    WareInfoService wareInfoService;

    @Test
    void contextLoads() {
        List<WareInfoEntity> list = wareInfoService.list();
        list.forEach(System.out::println);
    }

}
