package com.leeven.glmall.ware.dao;

import com.leeven.glmall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品库存
 * 
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 20:11:49
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {
	
}
