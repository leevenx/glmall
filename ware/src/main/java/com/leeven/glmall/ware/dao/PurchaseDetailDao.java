package com.leeven.glmall.ware.dao;

import com.leeven.glmall.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 20:11:50
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
