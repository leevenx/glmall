package com.leeven.glmall.ware.dao;

import com.leeven.glmall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 20:11:50
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
