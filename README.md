# glmall

#### 介绍
谷粒商城代码

#### 软件架构
软件架构说明

#### 整合spring cloud alibaba
##### nacos服务发现，参考：https://github.com/alibaba/spring-cloud-alibaba/blob/master/spring-cloud-alibaba-examples/nacos-example/nacos-discovery-example/readme-zh.md
> 1.添加依赖：spring-cloud-starter-alibaba-nacos-discovery。
> 2.@EnableDiscoveryClient

#### 整合openfeign及使用
> 1.添加依赖。2.在调用端增加接口类 @FeignClient("coupon")，编写接口。
> 3.把要调用的远程controller的接口签名复制到这个接口中来。
> 4.启用远程调用功能：@EnableFeignClients(basePackages = "com.leeven.glmall.member.feign")。


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
