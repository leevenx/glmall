package com.leeven.glmall.member;

import com.leeven.glmall.member.entity.MemberEntity;
import com.leeven.glmall.member.service.MemberService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MemberApplicationTests {

    @Autowired
    MemberService memberService;

    @Test
    void contextLoads() {
        List<MemberEntity> list = memberService.list();
        for (MemberEntity memberEntity : list) {
            System.out.println(memberEntity);
        }
    }

}
