package com.leeven.glmall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leeven.common.utils.PageUtils;
import com.leeven.glmall.member.entity.IntegrationChangeHistoryEntity;

import java.util.Map;

/**
 * 积分变化历史记录
 *
 * @author leeven
 * @email leeven@gmail.com
 * @date 2020-07-24 20:08:39
 */
public interface IntegrationChangeHistoryService extends IService<IntegrationChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

