package com.leeven.glmall.member.feign;

import com.leeven.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

// coupon 服务名
@FeignClient("coupon")
public interface CouponFeignService {
    @RequestMapping("coupon/coupon/list")
    R list(@RequestParam Map<String, Object> params);
}
